package com.example.demo.profile;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileService {
    private final ProfileRepository repository;

    @Autowired
    public ProfileService(ProfileRepository repository) {
        this.repository = repository;
    }

    public List<Profile> getProfiles() {
        return repository.findAll();
    }

    public void createProfile(Profile profile) {
        String email = profile.getEmail();
        List<Profile> found = repository.findByEmail(email);
        if (found.size() > 0)
            throw new IllegalStateException("User with specified email already exists");
        repository.save(profile);
    }
}
