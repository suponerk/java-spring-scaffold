## ENVIROINMENT

- docker
- java 11 (jdk)

## HOW TO START

- run postgres from scaffold container: `bash ./postgres/run.sh`
- connect to postgres and create new database named `users` : `sudo -u postgres createdb users`
