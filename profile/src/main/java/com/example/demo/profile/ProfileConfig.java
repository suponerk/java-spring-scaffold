package com.example.demo.profile;

import java.time.Month;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;

@Configuration
public class ProfileConfig {

    private void seedProfile(ProfileRepository repository) {
        Profile andrey1Profile = new Profile("Andrey1", "email1.example.com", LocalDate.of(1970, Month.JANUARY, 1));
        Profile andrey2Profile = new Profile("Andrey2", "email2.example.com", LocalDate.of(1980, Month.FEBRUARY, 1));
        Profile andrey3Profile = new Profile("Andrey3", "email3.example.com", LocalDate.of(2003, Month.MARCH, 1));
        repository.saveAll(List.of(andrey1Profile, andrey2Profile, andrey3Profile));
    }

    @Bean
    CommandLineRunner commandLineRunner(ProfileRepository repository) {
        return args -> seedProfile(repository);
    }

}
