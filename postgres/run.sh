#!/bin/sh
set -e

# install postgres
sudo apt install postgresql


# create new table 'users'
sudo -u postgres createdb users

# login in psql
# sudo -u postgres psql

# docker run --name jdbc-postgres -e POSTGRES_PASSWORD=password -d postgres